package com.atlassian.labs.hipchat.rest;


import com.atlassian.labs.hipchat.HipChatProxyClient;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/user/show")
public class HipChatUserProxy
{
    private HipChatProxyClient client;

    public HipChatUserProxy(PluginSettingsFactory pluginSettingsFactory, WebResourceUrlProvider webResourceUrlProvider,
                            HipChatProxyClient hipChatProxyClient)
    {
        this.client = hipChatProxyClient;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getUser(@QueryParam("user_id") String userId)
    {
        return Response.ok(client.getUser(userId)).build();
    }

}