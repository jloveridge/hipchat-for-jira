AJS.$(function ($) {
    var Room, Rooms, rooms, PageView, pageView, RoomListView, RoomListViewForInput;

    Room = Backbone.Model.extend({});
    Rooms = Backbone.Collection.extend({
        model:Room,
        initialize:function (rooms) {
            var checkedRooms = hcRoomIds.split(","),
                sortedRooms = _.sortBy(rooms, function (room) {
                    return room.name.toLowerCase();
                });
            return _.map(sortedRooms, function (room) {
                if (_.indexOf(checkedRooms, String(room.room_id)) > -1) {
                    room.checked = 'checked="checked"';
                } else {
                    room.checked = "";
                }
                return room;
            });

        },
        toObject:function () {
            return this.models.map(function (room) {
                return room.attributes
            })
        }
    });
    rooms = new Rooms(_.filter(hcRooms.rooms, function (room) {
        return !room.is_archived;
    }));

    RoomListView = Backbone.View.extend({
        el:$('#room-list'),
        template:_.template($('#rooms-tmpl').html()),
        initialize:function () {
            this.render();
        },
        render:function () {
            this.$el.html(this.template({rooms:rooms.toObject()}));
        }
    });

    RoomListViewForInput = Backbone.View.extend({
        template:_.template($('#room-names-tmpl').html()),
        initialize:function (rooms) {
            this.html = this.template({rooms:rooms});
        }
    });

    PageView = Backbone.View.extend({
        el:$('body'),
        initialize:function () {
            var roomListView = new RoomListView;
        },
        events:{
            "submit form":"submit"
        },
        submit:function (e) {
            var _self = $('input[name="roomsToNotify"]:checked'),
                labels = _.map(_self, function (room) {
                    return $(room).data('label')
                }),
                roomIds = _.map(_self, function (room) {
                    return $(room).val()
                });
                roomNamesList = new RoomListViewForInput(labels);
            console.log("ROOMS:",roomNamesList.html);
            $('<input type="hidden" name="roomsToNotifyStr" value="' + roomIds.join(',') + '">').appendTo(e.currentTarget);
            $(roomNamesList.html).appendTo(e.currentTarget);
        }

    });
    pageView = new PageView;
})
