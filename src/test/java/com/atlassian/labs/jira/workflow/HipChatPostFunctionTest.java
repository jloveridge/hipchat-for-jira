package com.atlassian.labs.jira.workflow;

import com.atlassian.jira.issue.MutableIssue;
import org.junit.Test;

public class HipChatPostFunctionTest
{
    public static final String MESSAGE = "my message";

    protected HipChatPostFunction function;
    protected MutableIssue issue;

    @Test
    public void testNoop() throws Exception {
        assert true;
    }

//    @Before
//    public void setup(ConfigurationManager configurationManager, WebResourceUrlProvider webResourceUrlProvider) {
//
//        issue = createPartialMockedIssue();
//        issue.setDescription("");
//
//        function = new HipChatPostFunction(configurationManager,webResourceUrlProvider) {
//            protected MutableIssue getIssue(Map transientVars) throws DataAccessException {
//                return issue;
//            }
//        };
//    }
//
//    @Test
//    public void testNullMessage() throws Exception
//    {
//        Map transientVars = Collections.emptyMap();
//
//        function.execute(transientVars,null,null);
//
//        assertEquals("message should be empty","",issue.getDescription());
//    }
//
//    @Test
//    public void testEmptyMessage() throws Exception
//    {
//        Map transientVars = new HashMap();
//        transientVars.put("messageField","");
//        function.execute(transientVars,null,null);
//
//        assertEquals("message should be empty","",issue.getDescription());
//    }
//
//    @Test
//    public void testValidMessage() throws Exception
//    {
//        Map transientVars = new HashMap();
//        transientVars.put("messageField",MESSAGE);
//        function.execute(transientVars,null,null);
//
//        assertEquals("message not found",MESSAGE,issue.getDescription());
//    }

//    private MutableIssue createPartialMockedIssue() {
//        GenericValue genericValue = mock(GenericValue.class);
//        IssueManager issueManager = mock(IssueManager.class);
//        ProjectManager projectManager = mock(ProjectManager.class);
//        VersionManager versionManager = mock(VersionManager.class);
//        IssueSecurityLevelManager issueSecurityLevelManager = mock(IssueSecurityLevelManager.class);
//        ConstantsManager constantsManager = mock(ConstantsManager.class);
//        SubTaskManager subTaskManager = mock(SubTaskManager.class);
//        AttachmentManager attachmentManager = mock(AttachmentManager.class);
//        LabelManager labelManager = mock(LabelManager.class);
//        ProjectComponentManager projectComponentManager = mock(ProjectComponentManager.class);
//        UserManager userManager = mock(UserManager.class);
//
//        return new IssueImpl(genericValue, issueManager, projectManager, versionManager, issueSecurityLevelManager, constantsManager, subTaskManager, attachmentManager, labelManager, projectComponentManager, userManager);
//    }

}
